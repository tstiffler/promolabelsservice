﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace PromoLabels
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "PromoLabelsService" in both code and config file together.
    public class PromoLabelsService : IPromoLabelsService
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {ShipData.0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        public string GetZPLForPromoLabel(ShipData shipData)
        {
            //return DefaultZPL();
            return DynamicZPL(shipData);
        }

        private string DefaultZPL()
        {
            string defaultZPL = @"
                ^XA

                ^FX Opening Text
                ^CF0,100, 100
                ^FO0,100^FB812,1,0,C^FDSAVE NOW^FS  

                ^FX Percent Off
                ^CFR,200,200
                ^FO0,170^FB812,1,0,C^FD20% OFF^FS

                ^FX Secondary Text
                ^CF00,90
                ^FO50, 350^FB712,2,0,C^FDON ALL FILTERS FOR YOUR HOME^FS

                ^FX Line
                ^FO50,550^GB712,0,4,^FS

                ^FX Products, First Column
                ^CFS
                ^FO50, 575^FB366,2,0,C^FDFRIDGE FILTERS^FS
                ^FO50, 615^FB366,2,0,C^FDHUMIDIFIER FILTERS^FS
                ^FO50, 655^FB366,2,0,C^FDPOOL & SPA FILTERS^FS
                ^FO50, 695^FB366,2,0,C^FDAIR CLEANERS^FS
                ^FO50, 735^FB366,2,0,C^FDSEDIMENT FILTERS^FS

                ^FX Products, Second Column
                ^CFS
                ^FO416, 575^FB366,2,0,C^FDFRIDGE FILTERS^FS
                ^FO416, 615^FB366,2,0,C^FDHUMIDIFIER FILTERS^FS
                ^FO416, 655^FB366,2,0,C^FDPOOL & SPA FILTERS^FS
                ^FO416, 695^FB366,2,0,C^FDAIR CLEANERS^FS
                ^FO416, 735^FB366,2,0,C^FDSEDIMENT FILTERS^FS

                ^FX DiscountCode
                ^CFV,60,60
                ^FO0,875^FB812,1,0,C^FDUse code: RETURN20 at^FS
                ^FO0,935^FB812,1,0,C^FDDiscountFilters.com^FS

                ^FX Product Name
                ^CFV,0,10
                ^FO50,1100^FDGE MWF^FS
                ^CFU,0,5
                ^FO50,1180^FB366,2,0,L^FDREFRIGERATOR WATER FILTER^FS

                ^FX Product Name
                ^CFR,0,5
                ^FO50,1350^FB366,5,0,L^FDFor more information, reordering or installation instructions, visit us at www.DiscountFilter.com or scan the code.^FS

                ^FX QR Instructions
                ^CFS,0,40
                ^FO500,1200^FB250,4,0,C^FDSCAN TO REORDER^FS

                ^FX QR Code
                ^FO500,1275
                ^BQN,2,10
                ^FDMM,Awww.discountfilters.com^FS

                ^FX URL
                ^FX QR Instructions
                ^CFR,0,14
                ^FO50,1550^FB712,4,0,R^FDwww.discountfilters.com/peel/p82610/^FS


                ^XZ
                ";

            return defaultZPL;
        }

        private string DynamicZPL(ShipData shipData)
        {
            //string dynamicZPL = $@"
            //    ^XA

            //    ^FX Opening Text
            //    ^CF0,100, 100
            //    ^FO0,100^FB812,1,0,C^FD{shipData.OpeningPromoText}^FS  

            //    ^FX Percent Off
            //    ^CFR,200,200
            //    ^FO0,170^FB812,1,0,C^FD{shipData.PercentOff}% OFF^FS

            //    ^FX Secondary Text
            //    ^CF00,90
            //    ^FO50, 350^FB712,2,0,C^FD{shipData.SecondaryPromoText}^FS

            //    ^FX Line
            //    ^FO50,550^GB712,0,4,^FS

            //    ^FX Products, First Column
            //    ^CFS
            //    ^FO50, 575^FB366,2,0,C^FD{shipData.ListProducts[0]}^FS
            //    ^FO50, 615^FB366,2,0,C^FD{shipData.ListProducts[1]}^FS
            //    ^FO50, 655^FB366,2,0,C^FD{shipData.ListProducts[2]}^FS
            //    ^FO50, 695^FB366,2,0,C^FD{shipData.ListProducts[3]}^FS
            //    ^FO50, 735^FB366,2,0,C^FD{shipData.ListProducts[4]}^FS

            //    ^FX Products, Second Column
            //    ^CFS
            //    ^FO416, 575^FB366,2,0,C^FD{shipData.ListProducts[5]}^FS
            //    ^FO416, 615^FB366,2,0,C^FD{shipData.ListProducts[6]}^FS
            //    ^FO416, 655^FB366,2,0,C^FD{shipData.ListProducts[7]}^FS
            //    ^FO416, 695^FB366,2,0,C^FD{shipData.ListProducts[8]}^FS
            //    ^FO416, 735^FB366,2,0,C^FD{shipData.ListProducts[9]}^FS

            //    ^FX DiscountCode
            //    ^CFV,60,60
            //    ^FO0,875^FB812,1,0,C^FDUse code: {shipData.PromoCode} at^FS
            //    ^FO0,935^FB812,1,0,C^FD{shipData.Website}^FS

            //    ^FX Product Name
            //    ^CFV,0,10
            //    ^FO50,1100^FD{shipData.ProductName}^FS
            //    ^CFU,0,1
            //    ^FO50,1180^FB366,3,0,L^FD{shipData.ProductDescription}^FS

            //    ^FX Product Name
            //    ^CFR,0,5
            //    ^FO50,1350^FB366,5,0,L^FD{shipData.ReorderInstructions}^FS

            //    ^FX QR Instructions
            //    ^CFS,0,40
            //    ^FO500,1200^FB250,4,0,C^FD{shipData.QRCodeInstructions}^FS

            //    ^FX QR Code
            //    ^FO500,1275
            //    ^BQN,2,10
            //    ^FDMM,A{shipData.QRCode}^FS

            //    ^FX URL
            //    ^FX QR Instructions
            //    ^CFR,0,14
            //    ^FO50,1550^FB712,4,0,R^FD{shipData.QRCode}^FS


            //    ^XZ
            //    ";

            string dynamicZPL = $@"
                ^XA

                ^FX Product Name
                ^CF{shipData.Size_productName}
                ^FO50,100^FD{shipData.ProductName}^FS
                ^CF{shipData.Size_productDescription}
                ^FO50,180^FB366,{shipData.Lines_productDescription},0,L^FD{shipData.ProductDescription}^FS

                ^FX Reorder Instructions
                ^CFR,0,3
                ^FO50,350^FB366,5,0,L^FD{shipData.ReorderInstructions}^FS

                ^FX QR Instructions
                ^CFS,0,40
                ^FO500,200^FB250,4,0,C^FD{shipData.QRCodeInstructions}^FS

                ^FX QR Code
                ^FO500,100
                ^BQN,2,10
                ^FDMM,A{shipData.QRCode}^FS

                ^FX URL
                ^FX QR Instructions
                ^CFR,0,14
                ^FO50,550^FB712,4,0,R^FD{shipData.QRCode}^FS


                ^XZ
                ";

            return dynamicZPL;
        }
    }
}
