﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace PromoLabels
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPromoLabelsService" in both code and config file together.
    [ServiceContract]
    public interface IPromoLabelsService
    {
        [OperationContract]
        string GetData(int value);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        [OperationContract]
        string GetZPLForPromoLabel(ShipData shipData);
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project.After building the project, you can directly use the data types defined there, with the namespace "PromoLabels.ContractType".
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }

    [DataContract]
    public class ShipData
    {
        private string openingPromoText;
        private double percentOff;
        private string secondaryPromoText;
        private bool includeLine;
        private bool includeProducts;
        private List<string> listProducts;
        private string promoCode;
        private string website;
        private string productName;
        private string productDescription;
        private string qrCode;
        private string qrCodeInstructions;
        private string reorderInstructions;

        private string size_productName;
        private string size_productDescription;
        private string lines_productDescription;


        [DataMember]
        public string OpeningPromoText
        {
            get { return openingPromoText; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    openingPromoText = "Save Now";
                }
                else
                {
                    openingPromoText = MakeUpper(value);
                }
            }
        }

        [DataMember]
        public double PercentOff
        {
            get { return percentOff; }
            set
            {
                if (value >= 0 && value <= 100)
                {
                    percentOff = value;
                }
                else
                {
                    percentOff = 20.0;
                }
            }
        }

        [DataMember]
        public string SecondaryPromoText
        {
            get { return secondaryPromoText; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    secondaryPromoText = "On all filters for your home";
                }
                else
                {
                    secondaryPromoText = MakeUpper(value);
                }
            }
        }

        [DataMember]
        public bool IncludeLine
        {
            get { return includeLine; }
            set { includeLine = value; }
        }

        [DataMember]
        public bool IncludeProducts
        {
            get { return includeProducts; }
            set { includeProducts = value; }
        }

        [DataMember]
        public List<string> ListProducts
        {
            get { return listProducts; }
            set
            {
                if (listProducts == null)
                {
                    listProducts = new List<string>();
                    listProducts.Add("FRIDGE FILTERS");
                    listProducts.Add("HUMIDIFIER FILTERS");
                    listProducts.Add("POOL & SPA FILTERS");
                    listProducts.Add("AIR CLEANERS");
                    listProducts.Add("SEDIMENT FILTERS");
                    listProducts.Add("UNDER SINK");
                    listProducts.Add("WHOLE HOUSE");
                    listProducts.Add("HUMIDIFIER");
                    listProducts.Add("CLEANING SUPPLIES");
                    listProducts.Add("UV BULBS");
                }

                if (value.Count == 1)
                {
                    listProducts = value;
                }
                else if (value.Count > 1)
                {
                    foreach (string product in value)
                    {
                        listProducts.Add(MakeUpper(product));
                    }
                }

                for (int x = listProducts.Count; x < 10; x++)
                {
                    listProducts.Add("");
                }
            }
        }

        [DataMember]
        public string PromoCode
        {
            get { return promoCode; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    promoCode = "RETURN20";
                }
                else
                {
                    promoCode = MakeUpper(value);
                }
            }
        }

        [DataMember]
        public string Website
        {
            get { return website; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    website = "DiscountFilters.com";
                }
                else
                {
                    website = MakeUpper(value);
                }
            }
        }

        [DataMember]
        public string ProductName
        {
            get { return productName; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    productName = "";
                }
                else if (value.Length > 27)
                {
                    //productName = value.Substring(0, 20);
                    productName = value;
                    Size_productName = "S";
                }
                else if (value.Length > 20)
                {
                    //productName = value.Substring(0, 20);
                    productName = value;
                    Size_productName = "U";
                }
                else
                {
                    productName = MakeUpper(value);
                }
            }
        }

        [DataMember]
        public string ProductDescription
        {
            get { return productDescription; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    productDescription = "";
                }
                else if (value.Length > 60)
                {
                    productDescription = MakeUpper(value);
                    Size_productDescription = "Q";
                    lines_productDescription = "6";
                }
                else if (value.Length > 45)
                {
                    productDescription = MakeUpper(value);
                    Size_productDescription = "S";
                    lines_productDescription = "4";
                }
                else
                {
                    productDescription = MakeUpper(value);
                }
            }
        }

        [DataMember]
        public string QRCode
        {
            get { return qrCode; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    qrCode = "www.DiscountFilters.com";
                }
                else
                {
                    qrCode = MakeUpper(value);
                }
            }
        }

        [DataMember]
        public string QRCodeInstructions
        {
            get { return qrCodeInstructions; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    qrCodeInstructions = "Scan to reorder";
                }
                else
                {
                    qrCodeInstructions = MakeUpper(value);
                }
            }
        }

        [DataMember]
        public string ReorderInstructions
        {
            get { return reorderInstructions; }
            set
            {
                if (String.IsNullOrEmpty(value))
                {
                    reorderInstructions = "For more information, reordering or installation instructions, visit us at www.DiscountFilters.com or scan the code.";
                }
                else
                {
                    reorderInstructions = MakeUpper(value);
                }
            }
        }

        [DataMember]
        public string Size_productName
        {
            get { return size_productName; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    size_productName = value;
                }
            }
        }

        [DataMember]
        public string Size_productDescription
        {
            get { return size_productDescription; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    size_productDescription = value;
                }
            }
        }

        [DataMember]
        public string Lines_productDescription
        {
            get { return lines_productDescription; }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    lines_productDescription = value;
                }
            }
        }

        private string MakeUpper(string originalText)
        {
            string newText = "";

            try
            {
                if (!String.IsNullOrEmpty(originalText))
                {
                    newText = originalText.ToUpper();
                }
            }
            catch (Exception ex)
            {

            }

            return newText;
        }
    }
}
